package ut.mobilesensing;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.text.Html;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kerstin on 29/11/2014.
 */
public class SensorInformation {
    private static SensorInformation instance;

    private SensorInformation(){

    }

    public static SensorInformation getInstance(){
        if (instance == null){
            instance = new SensorInformation();
        }
        return instance;
    }

    public String getNiceSensors(Activity activity){
        SensorManager mySManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensorList= mySManager.getSensorList(Sensor.TYPE_ALL);
        StringBuilder sb = new StringBuilder();
        Sensor forNow;
        for (int i = 0; i < sensorList.size(); i++){
            forNow = sensorList.get(i);
            sb.append(forNow.getName());
            sb.append("\r\n");
            sb.append("---------\n");
        }
        return sb.toString();
    }

    public int getSensorCount(Activity activity)
    {
        SensorManager mySManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensorList= mySManager.getSensorList(Sensor.TYPE_ALL);
        return sensorList.size();
    }

    public ArrayList getAPInformation(Activity activity)
    {
        ArrayList answer = new ArrayList();
        WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> apList = wifiManager.getScanResults();


        for (ScanResult result : apList)
        {
            StringBuilder sb = new StringBuilder();
            sb.append("<b>SSID:</b> ");
            sb.append(result.SSID);
            sb.append("<br>");

            sb.append("<b>BSSID:</b> ");
            sb.append(result.BSSID);
            sb.append("<br>");

            sb.append("<b>Frequency: </b>");
            sb.append(result.frequency);
            sb.append("<br>");

            sb.append("<b>Signal: </b>");
            sb.append(result.level);
            sb.append("dBm <br>");

            sb.append("<b>Supported capabilities: </b>");
            sb.append(result.capabilities);
            //sb.append("<br>");

            answer.add(Html.fromHtml(sb.toString()));


        }

        return answer;
    }

   /* public String getAPInformation(Activity activity)
    {
        WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> apList = wifiManager.getScanResults();

        StringBuilder sb = new StringBuilder();
        for (ScanResult result : apList)
        {
            sb.append("SSID: ");
            sb.append(result.SSID);
            sb.append("\n");

            sb.append("BSSID: ");
            sb.append(result.BSSID);
            sb.append("\n");

            sb.append("Frequency: ");
            sb.append(result.frequency);
            sb.append("\n");

            sb.append("Signal: ");
            sb.append(result.level);
            sb.append("dBm \n");

            sb.append("Supported capabilities: ");
            sb.append(result.capabilities);
            sb.append("\n");

            sb.append("------------\n");


        }

        return sb.toString();
    }*/
}
