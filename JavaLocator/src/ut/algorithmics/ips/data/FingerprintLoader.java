package ut.algorithmics.ips.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class FingerprintLoader 
{
	public static FingerprintMeasure[] loadBatchData(String trainingFile) throws IOException
	{
		HashMap<String, ArrayList<FingerprintMeasureEntry>> measures = new HashMap<>();
		BufferedReader br = new BufferedReader(new FileReader(new File(trainingFile)));
		String line = "";
		while((line = br.readLine())!= null)
		{
			String[] measureParts = line.split(" ");
			String location = measureParts[1];
			FingerprintMeasureEntry entry = new FingerprintMeasureEntry(null, null,Double.parseDouble(measureParts[0]));
			measures.putIfAbsent(location, new ArrayList<FingerprintMeasureEntry>());

			ArrayList<FingerprintMeasureEntry> storedEntry = measures.remove(location);
			storedEntry.add(entry);
			measures.put(location, storedEntry);
		}
		Iterator<Entry<String, ArrayList<FingerprintMeasureEntry>>> measureEntries = measures.entrySet().iterator();
		ArrayList<FingerprintMeasure> measureAL = new ArrayList<>(100);
		while(measureEntries.hasNext())
		{
			Entry<String, ArrayList<FingerprintMeasureEntry>> hashmapEntry = measureEntries.next();
			FingerprintMeasureEntry[] entryArray = new FingerprintMeasureEntry[hashmapEntry.getValue().size()];
			hashmapEntry.getValue().toArray(entryArray);
			FingerprintMeasure measure = new FingerprintMeasure(hashmapEntry.getKey(),	entryArray);
			measureAL.add(measure);
		}
		
		FingerprintMeasure[] response = new FingerprintMeasure[measureAL.size()];
		measureAL.toArray(response);
		return response;
	}
}
