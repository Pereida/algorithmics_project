package  ut.geolocalization;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextReading {
	private Calculation calc = new Calculation();
	private static String path =Environment.getExternalStorageDirectory().getAbsolutePath()+"/MobileDemo/6/ownMeasurement/";
    private Distance dist;

	public static void main(String args[]) {
        try
        {
            TextReading test = new TextReading();
        }
        catch (Exception e)
        {
            Log.e("Error", "So bad!", e);
        }

	}

	public TextReading() throws IOException{
		Map<String, int[]> ourRoom = readAllLines(path);
		Map<String, Integer> roomMean = calc.getMean(ourRoom);
		dist = new Distance(roomMean);
	}

	public Map<String, int[]> readAllLines(String path) {
		// read in one folder (per direction) after the other
		Map<String, int[]> bssidLevel = new HashMap<String, int[]>();
		for (int i = 1; i < 5; i++) {
			System.out.println(path + i);
			File folders = new File(path + i);
			File[] files = folders.listFiles();
			System.out.println(files.toString());

			// read in four different files per direction
			for (int j = 0; j < files.length; j++) {
				BufferedReader bReader;
				try {
					bReader = new BufferedReader(new FileReader(files[j]));
					ArrayList<String> oneFile = new ArrayList<String>();
					String line = bReader.readLine();
					//read each line of the File, extracting BSSID and corresponding level
					while (line != null) {
						String bssid = getField(line, "BSSID: ", ';');
						int level = Integer.parseInt(getField(line, "Signal: ",
								'd'));
						System.out.println("BSSID: " + bssid + " level "
								+ level);
						if (bssidLevel.containsKey(bssid)) {
							int[] allLvls = bssidLevel.get(bssid);
							int[] newLvls = Arrays.copyOf(allLvls,
									allLvls.length + 1);
							newLvls[newLvls.length - 1] = level;
							bssidLevel.put(bssid, newLvls);
						} else {
							int[] levels = { level };
							bssidLevel.put(bssid, levels);
						}
						line = bReader.readLine();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bssidLevel;
	}

	private String getField(String line, String regex, char delimiter) {
		Pattern pattern = Pattern.compile(regex + "([^" + delimiter + "]+)");
		Matcher m = pattern.matcher(line);
		if (m.find()) {
			return m.group(1);
		}
		return null;
	}

    public String getBestMatch()
    {
        return dist.getBestMatch();
    }
}
