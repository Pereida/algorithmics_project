package ut.mobilesensing;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import ut.geolocalization.Distance;


public class DemoLocalizationActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_localization);
        getDistance();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_demo_localization, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getDistance()
    {
     try {
         String choice = "A150";

         Distance d150 = new Distance(choice, getAssets());
         String bestMatch = d150.getBestMatch();

         StringBuffer sb = new StringBuffer();
         sb.append(bestMatch);
         sb.append(" is the closest room to ");
         sb.append(choice);

         choice = "A126";
         Distance d126 = new  Distance(choice, getAssets());
         bestMatch = d126.getBestMatch();

         sb.append("\n");
         sb.append(bestMatch);
         sb.append(" is the closest room to ");
         sb.append(choice);

         TextView output = (TextView) findViewById(R.id.demo_text_output);
         output.setText(sb.toString());
     }
     catch (Exception e)
     {
         // Nothing
     }
    }
}
