SSID: eduroam
BSSID: dc:a5:f4:8d:1e:b0
Frequency: 2412
Signal: -58dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: dc:a5:f4:8d:1e:bf
Frequency: 5240
Signal: -52dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:c4:c1:8f
Frequency: 5220
Signal: -70dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:bf:15:af
Frequency: 5200
Signal: -69dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:bf:0a:2f
Frequency: 5180
Signal: -69dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:bf:15:a0
Frequency: 2437
Signal: -75dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:bf:0a:20
Frequency: 2462
Signal: -60dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: 424Space
BSSID: 4c:5e:0c:c6:75:7f
Frequency: 2412
Signal: -80dBm 
Supported capabilities: [WPA2-PSK-CCMP][ESS]
-----------
SSID: ut-public
BSSID: dc:a5:f4:8d:1e:b1
Frequency: 2412
Signal: -54dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: dc:a5:f4:8d:1e:be
Frequency: 5240
Signal: -51dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:c4:c1:8e
Frequency: 5220
Signal: -70dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:bf:15:ae
Frequency: 5200
Signal: -70dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:bf:15:a1
Frequency: 2437
Signal: -75dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:bf:0a:2e
Frequency: 5180
Signal: -69dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:bf:0a:21
Frequency: 2462
Signal: -64dBm 
Supported capabilities: [ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:c4:c1:80
Frequency: 2462
Signal: -87dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:c4:c1:81
Frequency: 2462
Signal: -86dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 10:8c:cf:79:2a:51
Frequency: 2412
Signal: -86dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:c4:bf:61
Frequency: 2412
Signal: -88dBm 
Supported capabilities: [ESS]
-----------