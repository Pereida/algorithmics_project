package ut.algorithmics.ips.data;

public class FingerprintMeasure 
{
	public String locationName;
	
	public FingerprintMeasureEntry[] entries;

	public FingerprintMeasure(String locationName, FingerprintMeasureEntry[] entries) 
	{
		super();
		this.locationName = locationName;
		this.entries = entries;
	}
	
	

}
