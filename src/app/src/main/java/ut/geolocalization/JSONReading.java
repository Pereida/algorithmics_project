package  ut.geolocalization;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
//import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.*;

public class JSONReading {

	private JsonParser parser = new JsonParser();

	// send json files to read one by one
	public Map<String, int[]> sendJSONFile(File[] roomFiles) {

		Map<String, int[]> bssidLvl = new HashMap<String, int[]>();

		if (roomFiles.length > 0) {
			for (int i = 0; i < roomFiles.length; i++) {

				try {
					// prints json file names
					System.out.println("File: \t" + roomFiles[i]);
					JsonElement jsonElement = parser.parse(new FileReader(
							roomFiles[i]));
					JsonObject jsonObject = jsonElement.getAsJsonObject();
					bssidLvl = readJSONFile(jsonObject, bssidLvl);
				} catch (FileNotFoundException e) {
					System.out.println(e.getMessage());
					// e.getStackTrace();
				} catch (IOException e) {
					System.out.println(e.getMessage());
					// e.getStackTrace();
				} catch (Exception e) {
					System.out.println(e.getMessage());
					// e.getStackTrace();
				}

			}
		}
		return bssidLvl;
	}

	// read a complete json file
	private Map<String, int[]> readJSONFile(JsonObject jsonObject,
			Map<String, int[]> storage) {

		Map<String, String> oneMeasure = new HashMap<String, String>();

		for (Entry<String, JsonElement> entry : jsonObject.entrySet()) {

			String key = entry.getKey();
			JsonElement value = entry.getValue();
			// System.out.println("What is this element?");

			if (value.isJsonObject()) {
				readJSONFile(value.getAsJsonObject(), storage);
			}
			// never called in our program
			/*
			 * else if (value.isJsonArray()) {
			 * System.out.println("It's a jsonarray"); JsonArray jsonArray =
			 * value.getAsJsonArray();
			 * 
			 * if (jsonArray.size() == 1) { readJSONFile((JsonObject)
			 * jsonArray.get(0), storage); } else { //prints json array name
			 * System.out.println(key); Iterator<JsonElement> msg =
			 * jsonArray.iterator(); while (msg.hasNext()) { ////prints json
			 * array values System.out.println(msg.next()); } } }
			 */
			else {
				oneMeasure.put(key, value.getAsString());
			}
		}
		String bssid = oneMeasure.get("BSSID");
		int level = Integer.parseInt(oneMeasure.get("level"));
		if (storage.containsKey(bssid)) {
			int[] allLvls = storage.get(bssid);
			int[] newLvls = Arrays.copyOf(allLvls, allLvls.length + 1);
			newLvls[newLvls.length - 1] = level;
			storage.put(bssid, newLvls);
		} else {
			int[] levels = { level };
			storage.put(bssid, levels);
		}
		// at the end of the program contains all BSSIDs received at this
		// location
		// and for each an array of up to three ints with the measured levels
		// System.out.println(storage.toString());
		return storage;
	}

	/*protected Map<String, Integer> handleOneJSONFile(File[] roomFiles, HashMap<String, Integer> theMeasure) {

		Map<String, String> oneMeasure = new HashMap<String, String>();

		JsonElement jsonElement;
		JsonObject jsonObject = null;
		try {
			jsonElement = parser.parse(new FileReader(roomFiles[0]));
			jsonObject = jsonElement.getAsJsonObject();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Entry<String, JsonElement> entry : jsonObject.entrySet()) {
			String key = entry.getKey();
			JsonElement value = entry.getValue();
			if (value.isJsonObject()) {
				handleOneJSONFile(value.getAsJsonObject(), theMeasure);
			}
			oneMeasure.put(key, value.getAsString());
		}
		String bssid = oneMeasure.get("BSSID");
		Integer level = Integer.parseInt(oneMeasure.get("level"));
		theMeasure.put(bssid, level);

		return theMeasure;
	}*/
}