package ut.mobilesensing;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends ActionBarActivity {
    public final static String EXTRA_MESSAGE = "aalto.mobilesensing.MESSAGE";

    //testing committing...
    //It works... only -4- 5 hours to see the same file :)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Called from the Sensor button
    public void showInformation(View view){
        Intent intent = new Intent(this, DisplaySensorInformation.class);
        startActivity(intent);
    }

    public void readSensor(View view)
    {
        Intent intent = new Intent(this, ReadSensorActivity.class);
        startActivity(intent);
    }

    public void getAccessPointList(View view)
    {
        Intent intent = new Intent(this, AccessPointListActivity.class);
        startActivity(intent);
    }

    public void getLocalizationOptions(View view)
    {
        Intent intent = new Intent(this, LocalizationServices.class);
        //String message = "Hello there!";
        //intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}
