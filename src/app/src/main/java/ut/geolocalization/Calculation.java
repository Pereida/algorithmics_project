package  ut.geolocalization;

import java.util.HashMap;
import java.util.Map;

public class Calculation {

	public Calculation() {
		super();
	}

	public Map<String, Integer> getMean(Map<String, int[]> room) {
		Map<String, Integer> res = new HashMap<String, Integer>();
		for (Map.Entry<String, int[]> entry : room.entrySet()) {
			String bssid = entry.getKey();
			int[] levels = entry.getValue();
			int sum = 0;
			for (int i = 0; i < levels.length; i++) {
				sum = sum + levels[i];
			}
			sum = sum / levels.length;
			res.put(bssid, sum);
		}
		System.out.println("Final Map: " +res);
		return res;
	}
}