package  ut.geolocalization;

public class Euclidean {
	private String roomnumber;
	private double distance;
	private double reliability;
	
	public Euclidean (String key, double amount, double share){
		roomnumber = key;
		distance = amount;
		reliability = share;
	}

	public String getRoomnumber() {
		return roomnumber;
	}

	public void setRoomnumber(String roomnumber) {
		this.roomnumber = roomnumber;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getReliability() {
		return reliability;
	}

	public void setReliability(double reliability) {
		this.reliability = reliability;
	}

}
