package  ut.geolocalization;
import android.content.res.AssetManager;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Collecting {
	private static Map<String, Map<String, Integer>> allRooms = new HashMap <String, Map<String, Integer>>();
	private static JSONReading reader = new JSONReading();
	private static Calculation calc = new Calculation();
    private final AssetManager assetManager;

    public static void main(String args[]) throws IOException {
		Collecting helper = new Collecting(null);
	}
		
	public Collecting(AssetManager mgr) throws IOException {
		this.assetManager = mgr;

        String filePath;
		for (int i = 1; i < 7; i++) {
			filePath = ""+i;
			int count = -1;
			System.out.println("Filepath: " + filePath);
			File[] jsonfiles = getJSONFiles(filePath, count);
			Map<String, Integer> oneRoom = calc.getMean(reader.sendJSONFile(jsonfiles));
			allRooms.put(filePath, oneRoom);
        }
		//allRooms now contains from 6-1 (141 - 112) all received BSSIDs and according mean levels
		System.out.println(allRooms);
	}

	// get only the .json files from a directory
	protected File[] getJSONFiles(String path, int counter) throws IOException {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath()+"/MobileDemo/";

		File folders = new File(root+path);
		File[] files = folders.listFiles();
		File[] jsonfiles = new File[files.length];

		System.out.println("Files read, in total: " + files.length);

		for (int i = 0; i < files.length; i++) {

			if (files[i].isFile()) {
				if (files[i].getName().endsWith(".json")
						|| files[i].getName().endsWith(".JSON")) {
					jsonfiles[++counter] = files[i];
					System.out.println(jsonfiles[counter]);
				}
			}
		}

		return jsonfiles;
	}
	

	public static Map<String, Map<String, Integer>> getAllRooms() {
		return allRooms;
	}

	public static void setAllRooms(Map<String, Map<String, Integer>> allRooms) {
		Collecting.allRooms = allRooms;
	}

}
