package ut.mobilesensing;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class ReadSensorActivity extends ActionBarActivity implements SensorEventListener{

    private SensorManager sManager;
    private Sensor infoGyro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_sensor);

        sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        infoGyro = sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        TextView titleTV = (TextView) findViewById(R.id.ReadSensorActivityTitle);
        titleTV.setText("Information for: "+infoGyro.getName());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_read_sensor, menu);
        return true;
    }


    public void onResume()
    {
        super.onResume();
        sManager.registerListener(this, infoGyro, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.values.length > 0)
        {
            TextView x_axis = (TextView) findViewById(R.id.x_axis);
            x_axis.setText(""+event.values[0]);
            if(event.values.length > 1)
            {
                TextView y_axis = (TextView) findViewById(R.id.y_axis);
                y_axis.setText(""+event.values[1]);
                if(event.values.length > 2)
                {
                    TextView z_axis = (TextView) findViewById(R.id.z_axis);
                    z_axis.setText(""+event.values[2]);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void onPause()
    {
        super.onPause();
        sManager.unregisterListener(this);
    }
}
