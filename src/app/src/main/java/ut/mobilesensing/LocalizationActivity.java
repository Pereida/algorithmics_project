package ut.mobilesensing;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import ut.geolocalization.TextReading;


public class LocalizationActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localization);
        getDistance();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_localization, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getDistance()
    {
        try {

            TextReading tReading = new TextReading();
            String bestMatch = tReading.getBestMatch();

            StringBuffer sb = new StringBuffer();
            sb.append(bestMatch);
            sb.append(" is the closest room to the selected room");
            //sb.append(choice);


            TextView output = (TextView) findViewById(R.id.localization_output);
            output.setText(sb.toString());
        }
        catch (Exception e)
        {
            Log.e("ERROR", "Awful!", e);
        }
    }
}
