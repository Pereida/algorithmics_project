package ut.algorithmics.ips.data;

public class FingerprintMeasureEntry 
{
	
	public String ssid;
	public String routerMac;
	public double signal;
	
	public FingerprintMeasureEntry(String eSsid, String eRouterMac, double eSignal) 
	{
		ssid = eSsid;
		routerMac = eRouterMac;
		signal = eSignal;
	}

}
