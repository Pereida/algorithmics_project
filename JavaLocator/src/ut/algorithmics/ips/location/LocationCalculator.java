package ut.algorithmics.ips.location;

import java.io.IOException;

import ut.algorithmics.ips.data.FingerprintLoader;
import ut.algorithmics.ips.data.FingerprintMeasure;

public class LocationCalculator {

	public String getClosestLocation(FingerprintMeasure[] trainingMeasures, FingerprintMeasure testMeasure,
			int distanceStrategy) {
		double minimumDistance = Integer.MAX_VALUE;
		String closestLocation = "Location not found";
		for (int i = 0; i < trainingMeasures.length; i++) {
			double currentDistance = getDistance(trainingMeasures[i], testMeasure, distanceStrategy);
			if (currentDistance < minimumDistance) {
				minimumDistance = currentDistance;
				closestLocation = trainingMeasures[i].locationName;
			}
		}

		return closestLocation;//+" is at "+minimumDistance+" millijaaks";
	}

	private double getDistance(FingerprintMeasure trainingMeasure, FingerprintMeasure testMeasure,
			int distanceStrategy) {
		switch (distanceStrategy) {
		case 1: // Euclidean distance
			return euclideanDistance(trainingMeasure, testMeasure);
		case 2:
			return squaredEuclideanDistance(trainingMeasure, testMeasure);
		case 3:
			return rankCorrelation(trainingMeasure, testMeasure);
		case 4:
			return cleverEuclideanDistance(trainingMeasure, testMeasure);
		default:
			return Double.MAX_VALUE;
		}

	}

	private double cleverEuclideanDistance(FingerprintMeasure trainingMeasure, FingerprintMeasure testMeasure) {
		// TODO Auto-generated method stub
		double squareDistance = 0.0;
		for (int i = 0; i < trainingMeasure.entries.length; i++) {
			if (!(trainingMeasure.entries[i].signal == -100) || !(testMeasure.entries[i].signal == -100))
				squareDistance += Math.pow(trainingMeasure.entries[i].signal - testMeasure.entries[i].signal, 2);
		}
		return Math.sqrt(squareDistance);
	}

	private double rankCorrelation(FingerprintMeasure trainingMeasure, FingerprintMeasure testMeasure) {
		// TODO Auto-generated method stub
		double squareDistance = 0.0;
		for (int i = 0; i < trainingMeasure.entries.length; i++) {
			if (!(trainingMeasure.entries[i].signal == -100) || !(testMeasure.entries[i].signal == -100))
				squareDistance += Math.pow(trainingMeasure.entries[i].signal - testMeasure.entries[i].signal, 2);
		}
		return -(1-((6*squareDistance)/(trainingMeasure.entries.length * (Math.pow(trainingMeasure.entries.length, 2)-1))));		
	}

	private double squaredEuclideanDistance(FingerprintMeasure trainingMeasure, FingerprintMeasure testMeasure) {
		double squareDistance = 0.0;
		for (int i = 0; i < trainingMeasure.entries.length; i++) {
			squareDistance += Math.pow(trainingMeasure.entries[i].signal - testMeasure.entries[i].signal, 2);
		}
		return squareDistance;
	}

	private double euclideanDistance(FingerprintMeasure trainingMeasure, FingerprintMeasure testMeasure) {
		double squareDistance = 0.0;
		for (int i = 0; i < trainingMeasure.entries.length; i++) {
			squareDistance += Math.pow(trainingMeasure.entries[i].signal - testMeasure.entries[i].signal, 2);
		}
		return Math.sqrt(squareDistance);
	}

	public static void main(String[] args) {
		try {
			FingerprintMeasure[] trainingData = FingerprintLoader.loadBatchData("../new data/parsed/X_train.txt");
			FingerprintMeasure[] testData = FingerprintLoader.loadBatchData("../new data/parsed/X_test.txt");
			
			FingerprintMeasure[] trainingDataRanks = FingerprintLoader.loadBatchData("../new data/parsed/X_train_ranks.txt");
			FingerprintMeasure[] testDataRanks = FingerprintLoader.loadBatchData("../new data/parsed/X_test_ranks.txt");
			LocationCalculator lc = new LocationCalculator();
			for (int i = 0; i < testData.length; i++) {
				System.out.println("Entry for test case " + testData[i].locationName);
				System.out.println("Euclidean distance: " + lc.getClosestLocation(trainingData, testData[i], 1));
				System.out
						.println("Squared euclidean distance: " + lc.getClosestLocation(trainingData, testData[i], 2));
				System.out.println("Rank correlation: " + lc.getClosestLocation(trainingDataRanks, testDataRanks[i], 3));
				System.out
						.println("Trimmed euclidean distance: " + lc.getClosestLocation(trainingData, testData[i], 4));
				System.out.println("-----------");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
