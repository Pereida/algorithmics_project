package ut.mobilesensing;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ShareActionProvider;

import java.util.ArrayList;


public class AccessPointListActivity extends ActionBarActivity {

    private ShareActionProvider shareActionProvider;
    private String wifi_info="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_point_list_activities);

        ListView accessPointList =(ListView)findViewById(R.id.access_point_list);
        ArrayList APInfo = SensorInformation.getInstance().getAPInformation(this);
        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.simplerow, APInfo);
        accessPointList.setAdapter(adapter);

        for (Object AP : APInfo)
        {
            wifi_info+=AP.toString();
            wifi_info+="\n-----------\n";
        }

        setTitle(R.string.access_point_list_title);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_access_point_list_activities, menu);

        //setShareIntent(sendIntent);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setShareIntent(Intent shareIntent) {
        if (shareActionProvider != null) {
            shareActionProvider.setShareIntent(shareIntent);
        }
    }

    public void share(View view)
    {


        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, wifi_info);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
