SSID: eduroam
BSSID: dc:a5:f4:8d:1e:b0
Frequency: 2412
Signal: -68dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: dc:a5:f4:8d:1e:bf
Frequency: 5240
Signal: -59dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:bf:15:af
Frequency: 5200
Signal: -62dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:bf:15:a0
Frequency: 2437
Signal: -70dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: 424Space
BSSID: 4c:5e:0c:c6:75:7f
Frequency: 2412
Signal: -59dBm 
Supported capabilities: [WPA2-PSK-CCMP][ESS]
-----------
SSID: ut-public
BSSID: dc:a5:f4:8d:1e:b1
Frequency: 2412
Signal: -62dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: dc:a5:f4:8d:1e:be
Frequency: 5240
Signal: -59dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:c4:c1:8e
Frequency: 5220
Signal: -84dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:bf:15:ae
Frequency: 5200
Signal: -62dBm 
Supported capabilities: [ESS]
-----------
SSID: ut-public
BSSID: 64:ae:0c:bf:15:a1
Frequency: 2437
Signal: -70dBm 
Supported capabilities: [ESS]
-----------
SSID: eduroam
BSSID: f8:4f:57:13:a0:b0
Frequency: 2462
Signal: -79dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: ut-public
BSSID: f8:4f:57:13:a0:b1
Frequency: 2462
Signal: -79dBm 
Supported capabilities: [ESS]
-----------
SSID: eduroam
BSSID: f8:4f:57:13:a0:bf
Frequency: 5220
Signal: -82dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: ut-public
BSSID: f8:4f:57:13:a0:be
Frequency: 5220
Signal: -81dBm 
Supported capabilities: [ESS]
-----------
SSID: eduroam
BSSID: 64:ae:0c:bf:0a:2f
Frequency: 5180
Signal: -84dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: dc:a5:f4:8d:2b:20
Frequency: 2412
Signal: -87dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: eduroam
BSSID: dc:a5:f4:8c:f7:c0
Frequency: 2412
Signal: -86dBm 
Supported capabilities: [WPA2-EAP-CCMP][ESS]
-----------
SSID: ut-public
BSSID: dc:a5:f4:8d:2b:21
Frequency: 2412
Signal: -87dBm 
Supported capabilities: [ESS]
-----------