package  ut.geolocalization;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class Distance {
	private Collecting collector;
	private JSONReading reader = new JSONReading();
	private Calculation calc = new Calculation(); 
	private static String path = "./A150";
    private AssetManager assetManager;

    private String bestMatch;
	
	public static void main (String args[]) throws IOException {
		Distance test = new Distance(path,null);
	}
	
	public Distance (String path, AssetManager mgr) throws IOException {
		//reading in and processing the room in question
        assetManager = mgr;
        collector = new Collecting(assetManager);

		File [] measurements = collector.getJSONFiles(path, -1);
		Map<String, int[]> ourRoom1 = reader.sendJSONFile(measurements);
		Map<String, Integer> ourRoom = calc.getMean(reader.sendJSONFile(measurements));
		System.out.println("Our Room looks like: "+ourRoom);
		//comparing with existing database
		bestMatch = euclid(ourRoom, collector.getAllRooms());
        Log.i("MATCH!", ">>>>>>>>>"+bestMatch+"<<<<<<<<<<<");
    }
	
	public Distance (Map<String, Integer> myMeasure) throws IOException {
        collector = new Collecting(null);
		String bestMatch = euclid(myMeasure, collector.getAllRooms());
		System.out.println("We are closest to: " +bestMatch);
	}
	
	protected String euclid(Map<String, Integer> current, Map<String, Map<String, Integer>> data){
		String closestMatch = "None";
		Euclidean[] allDistances = new Euclidean[data.size()];
		int counter = 0;
		//iterating over all reference rooms
		for (Map.Entry<String, Map<String, Integer>> room : data.entrySet()) {
			int howManyEntries = 0;
			double sumOfDifferences = 0;
		    String roomnr = room.getKey();
		    Map<String, Integer> referenceBssidlvl = room.getValue();
		    //iterating over each BSSID in the current room, calculating difference if possible
		    for (Map.Entry<String, Integer> thisRoom : current.entrySet()){
		    	String myBssid = thisRoom.getKey();
		    	int myLvl = Math.abs(thisRoom.getValue());
		    	if (referenceBssidlvl.containsKey(myBssid)){
		    		int refLvl = Math.abs(referenceBssidlvl.get(myBssid));
		    		sumOfDifferences = sumOfDifferences + Math.abs(myLvl - refLvl);
		    		howManyEntries++;
		    	}
		    }
		    System.out.println("Sum of Differences for " + roomnr + " is " + sumOfDifferences);
		    System.out.println("My room has "+howManyEntries+" fitting entries with the reference room.");
		    //saving Roomnr and corresponding Euc.Dist. in an array for comparison
		    double distance = sumOfDifferences/howManyEntries;
		    System.out.println("Size of current room is: " + current.size());
		    double fractionSharedAPs = (double) howManyEntries/current.size();
		    Euclidean thisEuclidean = new Euclidean(roomnr, distance, fractionSharedAPs);
		    allDistances[counter] = thisEuclidean;
		    counter++;
		}
		// iterate (?) to find smallest distance, print Roomnumber with switch/case?
		Euclidean smallestOne = allDistances[0];
		System.out.println("Distance for Room " +smallestOne.getRoomnumber()+ " " + smallestOne.getDistance()+", reliability: " +smallestOne.getReliability());
		for (int i = 1; i < allDistances.length; i++){
			System.out.println("Distance for Room " + allDistances[i].getRoomnumber()+ " is: " +allDistances[i].getDistance()+", reliability: " +allDistances[i].getReliability());
			if (allDistances[i].getDistance() < smallestOne.getDistance()){
				smallestOne = allDistances[i];
			}
		}
		//assigning the correct roomnumber based on the filepath
		switch (smallestOne.getRoomnumber()){
			case "1": closestMatch = "A112";
			break;
			case "2": closestMatch = "A118";
			break;
			case "3": closestMatch = "A124";
			break;
			case "4": closestMatch = "A130";
			break;
			case "5": closestMatch = "A136";
			break;
			case "6": closestMatch = "A141";
			break;
		}
		return closestMatch;
	}
	
	/*not necessary anymore
	private Map<String, Integer> niceFormat(Map<String, int[]> badMap){
		Map <String, Integer> niceMap = new HashMap<String, Integer>();
		for(Map.Entry<String, int[]> data : badMap.entrySet()){
			String key = data.getKey();
			Integer level = data.getValue()[0];
			niceMap.put(key, level);
		}
		return niceMap;
	}
	*/
	public String getBestMatch()
    {
        return bestMatch;
    }

}
